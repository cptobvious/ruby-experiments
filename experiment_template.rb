#require "rubygems"
#require "bundler"

#ENV["BUNDLE_GEMFILE"] = File.expand_path("../Gemfile", __FILE__)
#Bundler.setup
#Bundler.require

module Experiment
  ITERATIONS = []
  # add class @@variables

  def self.setup(n)
    # initialize class variables
    # setup counts towards the startup time, but not the execution time
  end

  def self.run(n)
    # call to experiment function
  end
end
