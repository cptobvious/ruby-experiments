require "test/unit"

module Experiment
  ITERATIONS = [10]

  def self.setup(n)
    @@tree = Node.new
    @@tree.populate! n
  end

  def self.run(n)
    parent_node = @@tree
    puts n
    1.upto(n - 1) do |i|
      puts i
      assert_equal parent_node.left, @@tree
    end
  end

  class TestCase < Test::Unit::TestCase
    def test_foo
      puts "test"
      puts @@tree
      assert true
    end
  end

  class Node
    attr_accessor :left, :right

    def populate!(depth)
      return unless depth > 0

      self.left = Node.new
      self.left.populate! depth - 1
      self.right = Node.new
      self.right.populate! depth - 1
    end
  end
end
