module Experiment
  ITERATIONS = [1_000_000, 2_000_000]

  IDLE = 0
  WORKER = 1
  HANDLERA = 2
  HANDLERB = 3
  DEVICEA = 4
  DEVICEB = 5

  MAXTASKS = 6

  def self.setup(n)
    count = n

    @scheduler = Scheduler.new
    @scheduler.add_idle_task(IDLE, 0, nil, count)

    wkq = Packet.new(nil, WORKER, :work)
    wkq = Packet.new(wkq, WORKER, :work)
    @scheduler.add_worker_task(WORKER, 1000, wkq)

    wkq = Packet.new(nil, DEVICEA, :device)
    wkq = Packet.new(wkq, DEVICEA, :device)
    wkq = Packet.new(wkq, DEVICEA, :device)
    @scheduler.add_handler_task(HANDLERA, 2000, wkq)

    wkq = Packet.new(nil, DEVICEB, :device)
    wkq = Packet.new(wkq, DEVICEB, :device)
    wkq = Packet.new(wkq, DEVICEB, :device)
    @scheduler.add_handler_task(HANDLERB, 3000, wkq)

    @scheduler.add_device_task(DEVICEA, 4000, nil)
    @scheduler.add_device_task(DEVICEB, 5000, nil)
  end

  def self.run(n)
    @scheduler.schedule
    #puts "Queue Count = #{@scheduler.queue_count}"
    #puts "Hold Count = #{@scheduler.hold_count}"
  end

  class Scheduler
    attr_reader :hold_count, :queue_count

    def initialize
      @table = Array.new(MAXTASKS, nil)
      @list = nil
      @queue_count = 0
      @hold_count = 0
    end

    def hold_current
      @hold_count += 1
      @current_tcb.held
      @current_tcb.link
    end

    def queue(packet)
      if (task = @table.at(packet.id))
        @queue_count += 1
        packet.link = nil
        packet.id = @current_id
        task.check_priority_add(@current_tcb, packet)
      else
        task
      end
    end

    def release(id)
      task = @table.at(id)
      task.not_held
      if task.pri > @current_tcb.pri
        task
      else
        @current_tcb
      end
    end

    def schedule
      @current_tcb = @list
      while @current_tcb
        if @current_tcb.is_held_or_suspended?
          @current_tcb = @current_tcb.link
        else
          @current_id = @current_tcb.id
          @current_tcb = @current_tcb.run
        end
      end
    end

    def suspend_current
      @current_tcb.suspended
    end

    def add_device_task(id, pri, wkq)
      create_tcb(id, pri, wkq, DeviceTask.new(self))
    end

    def add_handler_task(id,pri,wkq)
      create_tcb(id, pri, wkq, HandlerTask.new(self))
    end

    def add_idle_task(id, pri, wkq, count)
      create_running_tcb(id, pri, wkq, IdleTask.new(self, 1, count))
    end

    def add_worker_task(id, pri, wkq)
      create_tcb(id, pri, wkq, WorkerTask.new(self, HANDLERA, 0))
    end

    def create_running_tcb(id, pri, wkq, task)
      create_tcb(id, pri, wkq, task)
      @current_tcb.set_running
    end

    def create_tcb(id, pri, wkq, task)
      @current_tcb = Tcb.new(@list, id, pri, wkq, task)
      @list = @current_tcb
      @table[id] = @current_tcb
    end
  end


  class DeviceTask
    def initialize(scheduler)
      @scheduler = scheduler
    end

    def run(packet)
      if packet
        @v1 = packet
        @scheduler.hold_current
      else
        if @v1
          pkt = @v1
          @v1 = nil
          @scheduler.queue(pkt)
        else
          @scheduler.suspend_current
        end
      end
    end
  end

  class HandlerTask
    def initialize(scheduler)
      @scheduler = scheduler
    end

    def run(packet)
      if packet
        if packet.kind == :work
          @v1 = packet.add_to(@v1)
        else
          @v2 = packet.add_to(@v2)
        end
      end
      if @v1
        if ((count = @v1.a1)  < 4 )
          if @v2
            v = @v2
            @v2 = @v2.link
            v.a1 = @v1.a2.at(count)
            @v1.a1 = count+1
            return @scheduler.queue(v)
          end
        else
          v = @v1
          @v1 = @v1.link
          return @scheduler.queue(v)
        end
      end
      @scheduler.suspend_current
    end
  end

  class IdleTask
    def initialize(scheduler, v1, v2)
      @scheduler = scheduler
      @v1 = v1
      @v2 = v2
    end

    def run(packet)
      if ( @v2 -= 1 ).zero?
        @scheduler.hold_current
      else
        pkt = if (@v1 & 1).zero?
          @v1 >>=  1
          DEVICEA
        else
          @v1 >>= 1
          @v1 ^= 0xD008
          DEVICEB
        end
        @scheduler.release pkt
      end
    end
  end

  class WorkerTask
    ALPHA = "0ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("")

    def initialize(scheduler, v1, v2)
      @scheduler = scheduler
      @v1 = v1
      @v2 = v2
    end

    def run(packet)
      if packet
        @v1 = if @v1 == HANDLERA
          HANDLERB
        else
          HANDLERA
        end
        packet.id = @v1
        packet.a1 = 0

        packet.a2.collect! do |x|
          @v2 += 1
          @v2 = 1 if @v2 > 26
          ALPHA.at(@v2)
        end
        @scheduler.queue packet
      else
        @scheduler.suspend_current
      end
    end
  end

  class Tcb
    RUNNING = 0b0   # 0
    RUNNABLE = 0b1  # 1
    SUSPENDED = 0b10  # 2
    HELD = 0b100   # 4
    SUSPENDED_RUNNABLE = SUSPENDED | RUNNABLE # 3
    NOT_HELD = ~HELD # -5

    attr_reader :link, :id, :pri

    def initialize(link, id, pri, wkq, task)
      @link = link
      @id = id
      @pri = pri
      @wkq = wkq
      @task = task
      @state = wkq ? 0b11 : 0b10
      #      @state = if wkq  then SUSPENDED_RUNNABLE else SUSPENDED end
      @old = nil
    end

    def check_priority_add(task, packet)
      if @wkq
        packet.add_to(@wkq)
      else
        @wkq = packet
        @state |= 0b1 # RUNNABLE
        return self if @pri > task.pri
      end
      task
    end

    def run
      pkt = if @state == 0b11 # 3 # SUSPENDED_RUNNABLE
        @old = @wkq
        @wkq = @old.link
        @state = @wkq ? 0b1 : 0b0 # RUNNABLE : RUNNING
        @old
      end
      @task.run pkt
    end

    def set_running
      @state = 0b0 # RUNNING
    end

    def suspended
      @state |= 0b10 # 2 # SUSPENDED
      self
    end

    def held
      @state |= 0b100 # 4 #HELD
    end

    def not_held
      @state &= -5 # NOT_HELD
    end

    PRECOMP = (0..5).collect do |state|
      (state & 0b100 ) != 0 || state == 0b10
      #    (state & HELD) != 0 || state == SUSPENDED
    end

    def is_held_or_suspended?
      PRECOMP.at(@state)
    end
  end

  class Packet
    attr_accessor :link, :id, :kind, :a1
    attr_reader :a2

    def initialize(link, id, kind)
      @link = link
      @id = id
      @kind = kind
      @a1 = 0
      @a2 = Array.new(4,0)
    end

    def add_to(queue)
      @link = nil
      unless queue
        self
      else
        next_packet = queue
        while (peek = next_packet.link)
          next_packet = peek
        end
        next_packet.link = self
        queue
      end
    end
  end
end
