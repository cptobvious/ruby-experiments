# http://rosettacode.org/wiki/LZW_compression#Ruby

module Experiment
  ITERATIONS = [10_000, 20_000]

  def self.setup(n)
    @@string = "TOBEORNOTTOBEORTOBEORNOT"
  end

  def self.run(n)
    n.times do
      self.decompress(self.compress(@@string))
    end
  end

  def self.compress(uncompressed)
    dict_size = 256
    dictionary = Hash[Array.new(dict_size) {|i| [i.chr, i.chr]}]

    w = ""
    result = []
    for c in uncompressed.split('')
      wc = w + c
      if dictionary.has_key?(wc)
        w = wc
      else
        result << dictionary[w]
        dictionary[wc] = dict_size
        dict_size += 1
        w = c
      end
    end

    result << dictionary[w] unless w.empty?
    result
  end

  def self.decompress(compressed)
    dict_size = 256
    dictionary = Hash[Array.new(dict_size) {|i| [i.chr, i.chr]}]

    w = result = compressed.shift
    for k in compressed
      if dictionary.has_key?(k)
        entry = dictionary[k]
      elsif k == dict_size
        entry = w + w[0, 1]
      else
        raise "Bad compressed k: %s" % k
      end
      result += entry

      dictionary[dict_size] = w + entry[0,1]
      dict_size += 1

      w = entry
    end
    result
  end
end
