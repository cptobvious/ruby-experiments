module Experiment
  ITERATIONS = [10_000_000, 50_000_000]

  def self.setup(n)
    @@result = 0
  end

  def self.run(n)
    n.times do
      @@result = Math::PI * Math::E % (Math::PI / Math::E)
    end
    @@result
  end
end
