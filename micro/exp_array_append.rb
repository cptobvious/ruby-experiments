module Experiment
  ITERATIONS = [30_000_000, 50_000_000]

  def self.setup(n)
    @@array = []
  end

  def self.run(n)
    n.times do |i|
      @@array << i
    end
    @@array
  end
end
