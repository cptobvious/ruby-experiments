module Experiment
  ITERATIONS = [20, 21, 22]

  def self.run(n)
    node = Node.new
    node.populate! n
    puts n
  end

  class Node
    attr_accessor :left, :right

    def populate!(depth)
      return unless depth > 0

      self.left = Node.new
      self.left.populate! depth - 1
      self.right = Node.new
      self.right.populate! depth - 1
    end
  end
end
