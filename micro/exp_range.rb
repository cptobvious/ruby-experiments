module Experiment
  ITERATIONS = [3_000_000, 6_000_000]

  def self.setup(n)
    @@start = Number.new(0)
    @@finish = Number.new(n)
    @@goal = Number.new(n - 1)
  end

  def self.run(n)
    (@@start..@@finish).include?(@@goal)
  end

  class Number
    attr_accessor :value

    def initialize(value)
      self.value = value
    end

    def <=>(other)
      value <=> other.value
    end

    def succ
      Number.new(value + 1)
    end
  end
end
