module Experiment
  ITERATIONS = [100_000, 200_000]

  def self.setup(n)
    @@array = []
  end

  def self.run(n)
    n.times do |i|
      @@array.insert 0, i
    end
    @@array
  end
end
