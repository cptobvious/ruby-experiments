module Experiment
  ITERATIONS = [1, 2, 5]

  def self.setup(n)
    @@tree = Node.new
    @@tree.populate! 20
  end

  def self.run(n)
    n.times do
      Marshal.load(Marshal.dump(@@tree))
    end
  end

  class Node
    attr_accessor :left, :right

    def populate!(depth)
      return unless depth > 0

      self.left = Node.new
      self.left.populate! depth - 1
      self.right = Node.new
      self.right.populate! depth - 1
    end
  end
end
