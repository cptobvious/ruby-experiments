module Experiment
  ITERATIONS = [1_000_000]

  def self.setup(n)
    @@strings = []
    n.times do |i|
      @@strings << i.to_s
    end
  end

  def self.run(n)
    n.times do |i|
      @@strings[i].to_sym
    end
  end
end
