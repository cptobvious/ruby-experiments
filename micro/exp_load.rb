module Experiment
  ITERATIONS = [5_000, 10_000]
  $VERBOSE = nil

  def self.run(n)
    n.times do
      load __FILE__
    end
  end
end
