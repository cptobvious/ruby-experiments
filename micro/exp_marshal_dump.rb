module Experiment
  ITERATIONS = [100_000]

  def self.setup(n)
    @@numbers = (0..1000).to_a
  end

  def self.run(n)
    n.times do
      Marshal.dump(@@numbers)
    end
  end
end
