module Experiment
  ITERATIONS = [2_000_000, 5_000_000]

  def self.run(n)
    n.times do
      (123_456.789).to_s.to_f
      123_456_789.to_s.to_i
    end
  end
end
