module Experiment
  ITERATIONS = [30_000_000]

  def self.run(n)
    n.times do
      block {0}
    end
  end

  def self.block
    yield
  end
end
