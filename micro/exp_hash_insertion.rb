module Experiment
  ITERATIONS = [1_000_000, 5_000_000]

  def self.setup(n)
    @@hash = {}
  end

  def self.run(n)
    n.times do |i|
      @@hash[i] = i
    end
  end
end
