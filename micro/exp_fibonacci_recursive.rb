module Experiment
  ITERATIONS = [30, 35]

  def self.run(n)
    fib n
  end

  def self.fib(n)
    if (n < 2)
      n
    else
      fib(n - 1) + fib(n - 2)
    end
  end
end
