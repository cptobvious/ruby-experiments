module Experiment
  ITERATIONS = [10_000_000, 50_000_000]

  def self.setup(n)
    @@x = E.new
    @@x.extend C
  end

  def self.run(n)
    n.times do
      @@x.foo
      @@x.bar
      @@x.baz
      @@x.bat
      @@x.baa
    end
    @@x
  end
end

module A
  def foo
  end
end

module B
  def bar
  end
end

module C
  def baa
  end
end

class D
  include A

  def baz
  end
end

class E < D
  include B

  def bat
  end
end
