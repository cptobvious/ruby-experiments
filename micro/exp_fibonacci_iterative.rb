module Experiment
  ITERATIONS = [100_000, 500_000]

  def self.setup(n)
    @@curr = 0
    @@succ = 1
  end

  def self.run(n)
    n.times do |i|
      @@curr, @@succ = @@succ, @@curr + @@succ
    end
  end
end
