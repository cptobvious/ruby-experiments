module Experiment
  ITERATIONS = [100_000]

  def self.setup(n)
    numbers = (0..1000).to_a
    @@dump = Marshal.dump(numbers)
  end

  def self.run(n)
    x = nil
    n.times do
      x = Marshal.load(@@dump)
    end
    x
  end
end
