module Experiment
  ITERATIONS = [30_000, 40_000]

  def self.run(n)
    2.upto(n) do |candidate|
      is_prime = true
      2.upto(candidate - 1) do |j|
        if candidate % j == 0
          is_prime = false
          break
        end
      end
      # puts i if is_prime
    end
  end
end
