# ----------------------------------------------------------------------
# The Great Computer Language Shootout
# http://shootout.alioth.debian.org/
#
# Based on D language implementation by Dave Fladebo
#
# Contributed by Anthony Borla
# ----------------------------------------------------------------------

module Experiment
  ITERATIONS = [2_500_000]

  def self.run(n)
    partial_sums n
  end

  def self.partial_sums(n)
    alt = 1.0
    s0 = s1 = s2 = s3 = s4 = s5 = s6 = s7 = s8 = 0.0

    1.upto(n) do |d|
      d = d.to_f
      d2 = d * d
      d3 = d2 * d
      ds = Math.sin(d)
      dc = Math.cos(d)

      s0 += (2.0 / 3.0) ** (d - 1.0)
      s1 += 1.0 / Math.sqrt(d)
      s2 += 1.0 / (d * (d + 1.0))
      s3 += 1.0 / (d3 * ds * ds)
      s4 += 1.0 / (d3 * dc * dc)
      s5 += 1.0 / d
      s6 += 1.0 / d2
      s7 += alt / d
      s8 += alt / (2.0 * d - 1.0)

      alt = -alt
    end

    return s1, s2, s3, s4, s5, s6, s7, s8
  end
end
