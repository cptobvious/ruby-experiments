module Experiment
  ITERATIONS = [100_000, 300_000]

  def self.setup(n)
    @@string = ""
  end

  def self.run(n)
    n.times do
      @@string += "x"
    end
    @@string
  end
end
