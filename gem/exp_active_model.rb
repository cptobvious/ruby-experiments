require "rubygems"
require "active_model"

module Experiment
  ITERATIONS = [200_000]

  def self.setup(n)

  end

  def self.run(n)
    n.times do |i|
      model = Model.new
      model.name = "Name#{i}"
      model.email = "foo#{i}@bar.com"
      model.content = "lorem ipsum" * 10
      model.valid?
    end
  end

  class Model
    include ActiveModel::Validations
    include ActiveModel::Conversion
    extend ActiveModel::Naming

    attr_accessor :name, :email, :content

    validates_presence_of :name
    validates_format_of :email, :with => /^[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}$/i
    validates_length_of :content, :maximum => 500

    def persisted?
      false
    end
  end
end
