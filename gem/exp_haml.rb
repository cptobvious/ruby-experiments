require "rubygems"
require "haml"

module Experiment
  ITERATIONS = [1_000, 2_000]
  TEMPLATE = <<-eos
%html
  %head
    %title= "Title".capitalize
  %body
    %ul
      - 1_000.times do |i|
        %li= i
  eos

  def self.run(n)
    n.times do
      Haml::Engine.new(TEMPLATE).render
    end
  end
end
