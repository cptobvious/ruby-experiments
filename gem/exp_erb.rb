require "erb"

module Experiment
  ITERATIONS = [1_000, 2_000]
  TEMPLATE = <<-eos
    <html>
      <head>
        <title><%= "Title".capitalize %></title>
      </head>
      <body>
        <ul>
          <%- 1_000.times do |i| %>
            <li><%= i %></li>
          <% end %>
        </ul>
      </body>
    </html>
  eos

  def self.run(n)
    n.times do
      ERB.new(TEMPLATE).result
    end
  end
end
