require "rubygems"
require "json/pure"

module Experiment
  ITERATIONS = [100_000, 200_000]

  def self.setup(n)
    @@hash = {
      :a => "Lorem Ipsum Dolor Sit Amet",
      :b => 42,
      :c => [
        :a => "Lorem Ipsum Dolor Sit Amet",
        :b => 42,
        :c => 3.1415
      ]
    }
  end

  def self.run(n)
    n.times do
      @@hash.to_json
    end
  end
end
