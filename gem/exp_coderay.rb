require "rubygems"
require "coderay"

module Experiment
  ITERATIONS = [1_000_000, 10_000_000]

  def self.setup(n)
    @@source_code = File.read("scientist.rb")
  end

  def self.run(n)
    n.times do
      CodeRay.scan(@@source_code, :ruby)
    end
  end
end
