if defined? RUBY_ENGINE && RUBY_ENGINE != "topaz"
  load "timeout_proxy.rb"
end

class ExperimentWrapper
  def initialize(args)
    @file = args.first
    @times = {}
  end

  def timeable(iteration)
    start = Time.now
    yield
    finish = Time.now
    @times[iteration] = finish - start
  end

  def start
    load @file # sets Experiment module

    Experiment::ITERATIONS.each do |n|
      puts n
      Experiment.setup(n) if Experiment.respond_to? :setup

      if Experiment.respond_to? :with_timeout
        timeable(n) do
          Experiment.with_timeout(60).run(n) rescue Timeout::Error
        end
      else
        timeable(n) do
          Experiment.run(n)
        end
      end
    end
    puts @times.inspect
    Marshal.dump(@times, File.new("results.dmp", "w"))
  end
end

experiment_wrapper = ExperimentWrapper.new(ARGV)

begin
  experiment_wrapper.start
rescue Exception => e
  # fail silently for now
  $stderr.puts "FAILING", e.message
end
