#!/usr/bin/env ruby
require "rvm"
require "date"
require "json"

EXPERIMENT_ITERATIONS = 3
RESULTS_FILENAME = "results.dmp"

class Scientist
  def initialize(rubies, experiments)
    @rubies = rubies
    @experiments = experiments
    puts "initialized with #{@rubies} and #{@experiments}"
    @results = {}
  end

  def timeable
    start = Time.now
    yield
    finish = Time.now
    finish - start
  end

  def experiment
    @rubies.each do |ruby|
      #next unless ruby.include?("jruby-1.6") || ruby.include?("jruby-1.7")
      #next unless ruby.include?("ree") || ruby.include?("2.0.0") || ruby.include?("1.9.3")
      #next unless ruby == "jruby-1.3.1"
      #next unless ruby.start_with?("ree")

      next unless RVM.use ruby

      puts ruby

      puts "Installing Gems"
      RVM.run "BUNDLE_GEMFILE=gem/Gemfile bundle"

      implementation_times = {}

      @experiments.each do |experiment_name|
        #next unless ["micro/exp_partial_sums.rb", "micro/exp_mergesort_hongli.rb"].include? experiment_name
        #next unless experiment_name == "micro/exp_range.rb"
        #next unless experiment_name.split("/").first == "gem"
        #next unless experiment_name.split("/").first == "macro"
        puts experiment_name

        iteration_times = {}
        startup_times = []

        EXPERIMENT_ITERATIONS.times do
          process = nil
          time = timeable do
            process = RVM.run "ruby experiment_wrapper.rb #{experiment_name}"
          end

          if File.exists?(RESULTS_FILENAME)
            times = Marshal.load(File.open(RESULTS_FILENAME, "r"))
            File.delete(RESULTS_FILENAME)

            puts times.inspect

            times.each do |iter, meas|
              iteration_times[iter] ||= []
              iteration_times[iter] << meas
            end

            startup_times << time - times.values.inject(:+) if times.values.any?
          else
            puts "error", process.stderr
          end

          puts "took #{time}s" #
        end

        implementation_times[experiment_name] ||= {}
        iteration_times.each do |iter, meas|
          implementation_times[experiment_name][iter] = {
            :execution_time => geometric_mean(meas),
            :startup_time => geometric_mean(startup_times),
            :execution_time_deviation => standard_deviation(meas),
            :startup_time_deviation => standard_deviation(startup_times),
            :measured_at => DateTime.now.to_s,
            :execution_time_best => meas.min,
            :execution_time_worst => meas.max
          }
        end
      end

      @results[ruby] = implementation_times
    end

    File.open("results.json", "w") do |file|
      file.write @results.to_json
    end
  end

  def arithmetic_mean(values)
    values.inject(&:+) / values.length.to_f
  end

  def geometric_mean(values)
    res = 1.0
    values.each {|value| res *= value}
    res ** (1.0 / values.size)
  end

  def standard_deviation(values)
    arith_mean = arithmetic_mean(values)
    calc = values.map {|v| (v - arith_mean) ** 2}.inject(&:+)
    Math.sqrt(calc / values.length.to_f)
  end
end

experiments = Dir.glob("micro/*.rb")
experiments += Dir.glob("macro/*.rb")
experiments += Dir.glob("gem/*.rb")

rubies = RVM.list_strings

rubies.select! {|ruby| ruby.include?("head")}

scientist = Scientist.new(rubies, experiments)
scientist.experiment
