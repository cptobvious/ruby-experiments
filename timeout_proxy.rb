require "timeout"

class TimeoutProxy
  include Timeout

  def initialize(receiver, timeout_in_seconds)
    @receiver = receiver
    @timeout_in_seconds = timeout_in_seconds
  end

  def method_missing(method, *args, &block)
    timeout(@timeout_in_seconds) do
      @receiver.send(method, *args, &block)
    end
  end
end

class Object
  def with_timeout(timeout_in_seconds)
    TimeoutProxy.new(self, timeout_in_seconds)
  end
end
